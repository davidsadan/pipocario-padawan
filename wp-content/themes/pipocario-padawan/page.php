<?php get_header();?>


	<!-- +++++ Welcome Section +++++ -->
	<div id="ww">
	    <div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
				<?php
				// Start the loop.
				if ( have_posts() ) : the_post();?>
				<h1><?php the_title();?></h1>
				<div class="wrapper_content">
					<?php the_content();?>
				</div>

				<?php
				endif;
				?>


					
					
				</div><!-- /col-lg-8 -->
			</div><!-- /row -->
	    </div> <!-- /container -->
	</div><!-- /ww -->
	
<?php get_footer(); ?>





